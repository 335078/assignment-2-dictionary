%include "lib.inc"
%include "dict.inc"
%include "words.inc"
%include "exit_codes.inc"
%define MAX_LENGTH 256
 
section .bss 
    buff: resb MAX_LENGTH

section .rodata
    key_not_found: db "Error: key not found.", 0
    reading_error: db "Error: too many chars for key.", 0

global _start

section .text
_start:
    mov rdi, buff
    mov rsi, MAX_LENGTH
    call read_line
    test rax, rax
    je .error_reading
    mov rdi, rax
    mov rsi, INDEX
    push rdx
    call find_word
    pop rdx
    test rax, rax
    je .key_error
    lea rdi, [rax + rdx + 9]
    call print_string
    xor rdi, rdi
    jmp exit
    .error_reading:	
        mov rdi, reading_error
        call print_error
        mov rdi, overflow_error
        jmp exit
    .key_error:
        mov rdi, key_not_found
        call print_error
        mov rdi, not_found
        jmp exit


