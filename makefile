ASM=nasm
ASMFLAGS=-felf64
LD=ld

main: main.o dict.o lib.o
	$(LD) -o $@ $^
%.o: %.asm
	$(ASM) $(ASMFLAGS) -o $@ $<

dict.o: dict.asm lib.inc

main.o: main.asm lib.inc dict.inc words.inc colon.inc

python: main
	python3 test.py
clean:
	rm -rf *.o maiin
clearConsole:
	clear
.PHONY: clean clearConsole python
