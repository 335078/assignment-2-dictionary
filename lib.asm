%define STDIN_FILENO 0
%define STDOUT_FILENO 1
%define READ 0
%define WRITE 1
%define EXIT 60

section .text

global exit
global string_length
global print_string
global print_char
global print_newline
global print_uint
global print_int
global print_hex
global string_equals
global read_char
global read_word
global parse_int
global parse_uint
global string_copy
global read_string
global print_error
global read_line



; Принимает код возврата и завершает текущий процесс
exit:
    mov rax, EXIT
    syscall
    ret

; Принимает указатель на нуль-терминированную строку, возвращает её длину
string_length:
    xor rax,rax
    .loop:
        cmp byte[rax+rdi],0 
        je .end
        inc rax ;i++
        jmp .loop
    .end:
        ret

; Принимает указатель на нуль-терминированную строку, выводит её в stdout
print_string:
    xor rax, rax
    push rdi
    call string_length
    pop rsi
    mov rdx, rax 
    mov rdi, STDOUT_FILENO
    mov rax, WRITE
    syscall
    ret
; Принимает код символа и выводит его в stdout
print_char:
    push rdi
    mov rdx, 1
    mov rsi, rsp
    pop rdi
    mov rax, WRITE      
    mov rdi, STDOUT_FILENO     
    syscall
    ret

; Переводит строку (выводит символ с кодом 0xA)
;ПЕРЕДЕЛАНО
print_newline:
    xor rax, rax
    mov rdi, `\n`
    jmp print_char

; Выводит беззнаковое 8-байтовое число в десятичном формате
; Совет: выделите место в стеке и храните там результаты деления
; Не забудьте перевести цифры в их ASCII коды.
print_uint:
    mov rsi, 10
    mov rax, rdi
    mov rdi, rsp
    dec rdi
    sub rsp, 24
    mov byte[rdi], 0
    .loop:
        xor rdx, rdx
        div rsi
        or dl, 48
        dec rdi
        mov [rdi], dl
        test rax, rax
        jnz .loop
        call print_string
        add rsp, 24
        ret


; Выводит знаковое 8-байтовое число в десятичном формате 
print_int:
    xor rax, rax
    test rdi, rdi
    jl .minus 
    jmp print_uint 

    .minus:
        push rdi  
        mov rdi, '-' 
        call print_char
        pop rdi
        neg rdi 
        jmp print_uint





; Принимает два указателя на нуль-терминированные строки, возвращает 1 если они равны, 0 иначе
string_equals:
    
    ; Инициализируем счетчик и флаг
    xor rcx, rcx ; счетчик
    xor rax, rax ; флаг

    .compare_loop:
        mov dl, [rdi+rcx] ; байт из первой строки
        cmp dl, [rsi+rcx] ; сравниваем с байтом из второй строки
        jne .not_equal ; если не равны, переходим к not_equal

        inc rcx ; увеличиваем счетчик
        test dl, dl ; проверяем на конец строки
        jne .compare_loop ; если не конец строки, продолжаем сравнение

    .equal:
        mov al, 1 ; устанавливаем флаг в 1 (строки равны)
        jmp .end_compare

    .not_equal:
        xor al, al ; устанавливаем флаг в 0 (строки не равны)

    .end_compare:
        ret



; Читает один символ из stdin и возвращает его. Возвращает 0 если достигнут конец потока
read_char:
    mov rax, READ
    mov rdi, STDIN_FILENO
    mov rdx, 1
    push 0 
    mov rsi, rsp
    syscall
    pop rax
    ret

; Принимает: адрес начала буфера, размер буфера
; Читает в буфер слово из stdin, пропуская пробельные символы в начале, .
; Пробельные символы это пробел 0x20, табуляция 0x9 и перевод строки 0xA.
; Останавливается и возвращает 0 если слово слишком большое для буфера
; При успехе возвращает адрес буфера в rax, длину слова в rdx.
; При неудаче возвращает 0 в rax
; Эта функция должна дописывать к слову нуль-терминатор
read_word:
    push r12 ; используем callee-saved регистры из-за частого read_char
    push r13
    push r14
    mov r12, rdi ;адрес начала строки
    mov r13, rsi ;размер буфера
    dec r13 
    xor r14, r14 ;счетчик
    .loop:
        call read_char
        test rax, rax
        je .end
        
        cmp rax, ' ' ;проверки на пустые символы, переход на .skip_whitespace
        je .skip_whitespace
        cmp rax, `\t`
        je .skip_whitespace
        cmp rax, `\n`
        je .skip_whitespace
        cmp r13, r14
        je .error
        
        mov [r12+r14], rax
        inc r14
        cmp r14, r13
        jge .error
        
        jmp .loop
    .skip_whitespace:
        test r14, r14
        je .loop
    .end:
        xor rax, rax
        mov [r12+r14], rax
        mov rax, r12
        mov rdx, r14
        pop r14
        pop r13
        pop r12
        ret
    .error:
        xor rax, rax
        xor rdx, rdx
        pop r14
        pop r13
        pop r12
        ret
    

; Принимает указатель на строку, пытается
; прочитать из её начала беззнаковое число.
; Возвращает в rax: число, rdx : его длину в символах
; rdx = 0 если число прочитать не удалось	
parse_uint:
    mov rax, 0         
    mov r9, 0
    mov r8, 10
    mov rcx, 0
    .loop:
    	movzx r9, byte [rdi+rcx]  
    	test r9, r9
    	je .end
 
    	cmp r9b, '0'          
        jl .end 
        
        cmp r9b, '9'
        jle .rt1
    .end:
    	mov rdx, rcx
        ret
    .rt1:
        mul r8
    	sub r9b, '0'
    	add rax, r9
    	inc rcx
    	jmp .loop
    	

parse_int:
    xor rax, rax
    xor rdx, rdx
    mov rcx, rdi 
    cmp byte[rcx], '-'
    je .neg
    jmp .pos
    .neg:
        inc rcx
        mov rdi, rcx
        push rcx
        call parse_uint
        pop rcx
        neg rax
        inc rdx
        ret
    .pos:
        mov rdi, rcx
        jmp parse_uint
        

; Принимает указатель на строку, указатель на буфер и длину буфера
; Копирует строку в буфер
; Возвращает длину строки если она умещается в буфер, иначе 0
string_copy:
    xor rax, rax 
    push rsi
    push rdi
    push rdx
    call string_length
    pop rdx
    pop rdi
    pop rsi
    xor rcx, rcx ;i=0
    
    cmp rax, rdx ; проверка на возможность копирования строки в буфер
    jg .buffer_too_small

    ; Копирование строки в буфер
    ;xor rcx, rcx
    .copy_loop:
    	cmp rcx, rax
    	jg .end_func_copy
        mov al, byte [rcx + rdi]
        mov byte [rcx + rsi], al
        inc rcx
        test al, al
        jnz .copy_loop



    .buffer_too_small:
        ; Очистка стека
        xor rax, rax
        ret

    .end_func_copy:
        ret
        
print_error:
    push rdi
    call string_length
    pop rdi
    mov rsi, rdi
    mov rdx, rax
    mov rax, 1 
    mov rdi, 2
    syscall
    ret

read_line:
        xor rcx, rcx           
    .loop:
        push rdi                
        push rsi
        push rcx
        call read_char        
        pop rcx               
        pop rsi
        pop rdi
        cmp rax, 0x20         
        je .space
        cmp rax, 0x9
        je .space
        cmp rax, 0xA
        je .newline
        cmp rax, 0              
        je .end
    .continue:    
        mov [rdi + rcx], rax    
        inc rcx                
        cmp rcx, rsi           
        jge .err
        jmp .loop
    .space:
        cmp rcx, 0             
        je .loop                
        jmp .continue
    .newline:
        cmp rcx, 0
        je .loop
        jmp .end
    .err:
        xor rax, rax            
        xor rdx, rdx           
        ret
    .end:
        xor rax, rax           
        mov [rdi + rcx], rax
        mov rax, rdi           
        mov rdx, rcx           
        ret                    

        
