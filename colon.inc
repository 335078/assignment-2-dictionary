%define INDEX 0
%macro colon 2
    %ifstr %1
        %ifid %2
            %2:
	        dq INDEX
	        db %1, 0
	%else
	    %error "Wrong label"
	%endif
    %else
        %error "Wrong data"
    %endif
%define INDEX %2
%endmacro
